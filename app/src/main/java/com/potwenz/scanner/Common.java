package com.potwenz.scanner;

import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.MifareClassic;
import java.math.BigInteger;

public class Common {

    private static final char[] BYTE2HEX=(
            "000102030405060708090A0B0C0D0E0F"+
                    "101112131415161718191A1B1C1D1E1F"+
                    "202122232425262728292A2B2C2D2E2F"+
                    "303132333435363738393A3B3C3D3E3F"+
                    "404142434445464748494A4B4C4D4E4F"+
                    "505152535455565758595A5B5C5D5E5F"+
                    "606162636465666768696A6B6C6D6E6F"+
                    "707172737475767778797A7B7C7D7E7F"+
                    "808182838485868788898A8B8C8D8E8F"+
                    "909192939495969798999A9B9C9D9E9F"+
                    "A0A1A2A3A4A5A6A7A8A9AAABACADAEAF"+
                    "B0B1B2B3B4B5B6B7B8B9BABBBCBDBEBF"+
                    "C0C1C2C3C4C5C6C7C8C9CACBCCCDCECF"+
                    "D0D1D2D3D4D5D6D7D8D9DADBDCDDDEDF"+
                    "E0E1E2E3E4E5E6E7E8E9EAEBECEDEEEF"+
                    "F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF").toCharArray();
    ;

    /**
     * The last detected tag.
     * Set by {@link #treatAsNewTag(Intent, Context)}
     */
    private static Tag mTag = null;

    /**
     * The last detected UID.
     * Set by {@link #treatAsNewTag(Intent, Context)}
     */
    private static byte[] mUID = null;

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();


    public static String convertByteArrayToDecString (byte[] b) {
        if (b != null) {
            BigInteger n = new BigInteger(1, b);
            return n.toString();
        } else {
            return "";
        }
    }

    public static void reverse(byte[] array) {
        if (array == null) {
            return;
        }
        int i = 0;
        int j = array.length - 1;
        byte tmp;
        while (j > i) {
            tmp = array[j];
            array[j] = array[i];
            array[i] = tmp;
            j--;
            i++;
        }
    }

    public static String treatAsNewTag(Intent intent, Context context) {

        Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
        MifareClassic mif = MifareClassic.get(tag);
        if (mif == null) {
            return "";
        }

        reverse(id);

        int before = (int) Long.parseLong(bytesToHexString(id) , 16);
        String result = String.format("%010d", before);

        return result;
    }


    public static String bytesToHexString(byte[] bytes) {
        if (bytes == null) return null;

        StringBuilder ret = new StringBuilder(2*bytes.length);

        for (int i = 0 ; i < bytes.length ; i++) {
            int b;

            b = 0x0f & (bytes[i] >> 4);

            ret.append("0123456789abcdef".charAt(b));

            b = 0x0f & bytes[i];

            ret.append("0123456789abcdef".charAt(b));
        }

        return ret.toString();
    }



}
